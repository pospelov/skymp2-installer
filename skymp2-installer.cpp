#include "stdafx.h"
#include "futuralightc.otf.h"
#include "sounds.zip.h"

// Relative to tempdir 
#define TEMP_FILE_1 "zzzrep_1.bin" // font.otf
#define TEMP_FILE_2 "zzzrep_2.bin" // archive.zip
#define TEMP_FILE_3 "zzzrep_3.bin" // version-info.json
#define TEMP_FILE_4 "zzzrep_4.bin" // ui sounds.zip
#define TEMP_FILE_5 "zzzrep_5.bin" // compare result (json)
#define TEMP_FILE_6 "zzzrep_6.bin" // installed commit sha (wstring) (referenced in skymp2 client!)
#define TEMP_FILE_7 "zzzrep_7.bin" // repo branches (json)
#define SKYRIM_SCREEN_JPG "zzzrep_a.bin"

// Relative to TESV.exe parent folder
#define LAUNCH_MOMENT_FILE "_skymp_launch_moment_.txt" // Skyrim invoked by launcher moment (time_t)
#define MAGIC_LAUNCH_MOMENT 108

#define CMDLINE_LAUNCHER "-help"
#define CMDLINE_CRASHDETECT "-crashdetect"

// Fullscreen fake Skyrim 
// Requires TAKE_SCREENSHOTS == true (ImguiManager.cpp in skymp2-client)
// Note: feature is not finished
#define CRASHDETECT_DUMMY false

void Log(const std::string &message) {
	static std::mutex m;
	std::lock_guard<std::mutex> l(m);

	char tempPath[MAX_PATH] = { 0 };
	GetTempPathA(MAX_PATH, tempPath);
	sprintf_s(tempPath, "%s/%s", tempPath, "_skymp-installer.log");
	std::ofstream f(tempPath, std::ios::app);
	static bool timestamped = false;
	if (!timestamped) {
		timestamped = true;
		time_t t = time(NULL);
		struct tm tm = *localtime(&t);
		char buf[128];
		sprintf_s(buf, "\n\nnow: %d-%d-%d %d:%d:%d", tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
		f << buf << std::endl;
	}
	f << message << std::endl;
}

#include <Windows.h>
#include <Wininet.h>
#include <iostream>
#include <fstream>
#pragma comment ( lib, "Wininet.lib" )

namespace {

	::HINTERNET netstart(std::ostream &err)
	{
		const ::HINTERNET handle =
			::InternetOpenW(0, INTERNET_OPEN_TYPE_DIRECT, 0, 0, 0);
		if (handle == 0)
		{
			const ::DWORD error = ::GetLastError();
			err
				<< "InternetOpen(): " << error << "."
				<< std::endl;
		}
		return (handle);
	}

	void netclose(::HINTERNET object, std::ostream &err)
	{
		const ::BOOL result = ::InternetCloseHandle(object);
		if (result == FALSE)
		{
			const ::DWORD error = ::GetLastError();
			err
				<< "InternetClose(): " << error << "."
				<< std::endl;
		}
	}

	::HINTERNET netopen(::HINTERNET session, ::LPCWSTR url, std::ostream &err)
	{
		const ::HINTERNET handle =
			::InternetOpenUrlW(session, url, 0, 0, INTERNET_FLAG_NO_CACHE_WRITE, 0);
		if (handle == 0)
		{
			const ::DWORD error = ::GetLastError();
			err
				<< "InternetOpenUrl(): " << error << "."
				<< std::endl;
		}
		return (handle);
	}

	void netfetch(::HINTERNET istream, std::ostream& ostream, std::ostream &err, std::function<void(int)> onReadBytes)
	{
		static const ::DWORD SIZE = 1024;
		::DWORD error = ERROR_SUCCESS;
		::BYTE data[SIZE];
		::DWORD size = 0;
		do {
			::BOOL result = ::InternetReadFile(istream, data, SIZE, &size);
			if (result == FALSE)
			{
				error = ::GetLastError();
				err
					<< "InternetReadFile(): " << error << "."
					<< std::endl;
			}
			onReadBytes(size);
			ostream.write((const char*)data, size);
		} while ((error == ERROR_SUCCESS) && (size > 0));
	}

}

#include <iostream>
#include <sstream>
//#include <boost/asio.hpp>

int g_counter = 0;

void MyURLDownloadToFileA(
	LPUNKNOWN            pCaller,
	LPCSTR              szURL,
	LPCSTR              szFileName,
	_Reserved_ DWORD                dwReserved,
	std::function<void(int)> cb,
	int depth = 0
) {
	g_counter++;
	auto wurl = toUtf16LE(szURL);
	const ::WCHAR *URL = wurl.data();
	std::string errS;
	std::ostringstream oerr(errS);
	const ::HINTERNET session = ::netstart(oerr);
	if (session != 0)
	{
		const ::HINTERNET istream = ::netopen(session, URL, oerr);
		if (istream != 0)
		{
			std::ofstream ostream(szFileName, std::ios::binary);
			if (ostream.is_open()) {
				int sumBytes = 0;
				::netfetch(istream, ostream, oerr, [&](int bytes) {
					sumBytes += bytes;
					if (cb) cb(sumBytes);
				});
			}
			else {
				oerr << "Could not open 'output.txt'." << std::endl;
			}
			::netclose(istream, oerr);
		}
		::netclose(session, oerr);
	}

	if (oerr.str().size()) {
		auto s = oerr.str();
		s += " " + std::to_string(g_counter);
		s += "\n\nИнформация по решению проблемы в канале instructions на нашем Discord-сервере.";
		MessageBoxA(0, s.data(), "Что-то пошло не так", MB_ICONERROR);
		std::exit(0);
	}
}

#define URLDownloadToFileA MyURLDownloadToFileA

void Log(const std::wstring &wmessage) {
	Log(toUtf8(wmessage));
}

enum PageId {
	kPageId_CrashDetectNormal,
	kPageId_Hello,
	kPageId_Folder,
	kPageId_Progress,
};

enum Button {
	// Bottom
	BUTTON_BACK,
	BUTTON_NEXT,
	BUTTON_FINISH,

	// Custom
	BUTTON_SELECT_FOLDER,

	BUTTON_COUNT
};

// Data
static LPDIRECT3DDEVICE9 g_pd3dDevice = nullptr;
static D3DPRESENT_PARAMETERS g_d3dpp;
static ImFont *g_font24 = nullptr;
static ImFont *g_font14 = nullptr;
static float g_alphaBuffers[BUTTON_COUNT] = { 0,0,0 };
static char g_tesvFolder[MAX_PATH] = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Skyrim";
static bool g_startAfterInstall = true;
static volatile bool g_isCrashdetect = false;
static HWND g_hwndCrashdetect = 0;
static float g_width = INSTA_WIDTH;
static float g_height = INSTA_HEIGHT;

const ImVec4 clear_color = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);

class Page
{
public:

	Page( const char *cap_, const char *txt_, std::function<void(Page &)> ctorTask = nullptr) {
		caption = cap_;
		text = txt_;
		if (ctorTask) ctorTask(*this);
	}

	const char *caption;
	const char *text;
	const char *buttons[3] = { u8"Назад", u8"Далее", u8"Отмена" };
	bool blocked[3] = { false, false, false };
	bool drawButton[3] = { true, true, true };
	float fontScale = 1.f;
	bool textHeight24 = false;
};

Page g_pages[] = {
	Page(
	u8"Что-то пошло не так. SkyMP перезапускается",
	/*u8"\nКажется, Skyrim аварийно завершил работу из-за критической ошибки.\n\n\
Отчёт об ошибке и гневное письмо отправлены разработчику. Пожалуйста, подождите несколько секунд.\n\n\
Автоматическая служба SkyMP уже восстанавливает игровую сессию."*/u8"Это займёт несколько секунд...",
		[](Page &p) { p.textHeight24 = false; for (size_t i = 0; i < std::size(p.drawButton); ++i) p.drawButton[i] = false; }
	),
	Page(
	u8"Вас приветствует мастер установки SkyMP",
	u8"The Elder Scrolls V: Skyrim - ролевая компьютерная игра с открытым миром, разработанная студией Bethesda Game Studios - обрела многопользовательский режим благодаря проекту SkyMP.\
		\nSkyMP - это глобальный проект, позволяющий многим людям одновременно окунуться в атмосферу мира древних свитков.\
		\n\n\nНажмите Далее, чтобы установить SkyMP на Ваш компьютер.",
		[](Page &p) { p.blocked[BUTTON_BACK] = true; }
	),
	Page(
		u8"Определить местоположение игры.",
		u8"Пожалуйста, укажите путь до корневой папки TES V: Skyrim.",
		[](Page &p) { p.buttons[BUTTON_NEXT] = u8"Установить"; }
	),
	Page(
		u8"Установка.",
		u8"Пожалуйста, не перезагружайте ПК и интернет-соединение во время установки.",
		[](Page &p) { p.blocked[BUTTON_BACK] = p.blocked[BUTTON_NEXT] = p.blocked[BUTTON_FINISH] = true; p.buttons[BUTTON_FINISH] = u8"Готово"; }
	)
};
int g_pageIndex = kPageId_Hello;

void WriteLaunchMoment(time_t t) {
	std::string f;
	if (!g_tesvFolder[0]) {
		f = LAUNCH_MOMENT_FILE;
	}
	else {
		f = (std::string)g_tesvFolder + "/" + LAUNCH_MOMENT_FILE;
	}

	std::ofstream(f) << std::to_string(t);
	Log("WriteLaunchMoment() " + f);
}

void SiriusStar(std::wstring name = L"") {
	Log("SiriusStar()");
	if (name.size() == 0) {
		auto gameFolder = toUtf16LE(g_tesvFolder);
		name = (gameFolder + L"/SKYMP2/loading.exe");
	}
	STARTUPINFO cif;
	PROCESS_INFORMATION pi;
	ZeroMemory(&cif, sizeof(STARTUPINFO));
	auto res = CreateProcess(name.data(), NULL,
		NULL, NULL, FALSE, DETACHED_PROCESS, NULL, NULL, &cif, &pi);
	if (res) WaitForSingleObject(pi.hProcess, INFINITE);
	Log("SiriusStar() exit");
}

// Returns zipPath
const char *DownloadSkympZip(const char *zipPath_, std::function<void(float)> callbackPercentage) {

	static char tempPath[MAX_PATH] = "", zipPath[MAX_PATH], viPath[MAX_PATH];
	if (!tempPath[0]) {
		GetTempPathA(MAX_PATH, tempPath);
		sprintf_s(zipPath, "%s/" TEMP_FILE_2, tempPath);
		sprintf_s(viPath, "%s/" TEMP_FILE_3, tempPath);
	}
	if (!zipPath_ || !zipPath_[0]) zipPath_ = zipPath;

	// Download version info
	URLDownloadToFileA(0, SKYMP2_VERINFO_URL, viPath, 0, 0);
	std::string viDump;
	std::ifstream(SKYMP2_VERINFO_URL) >> viDump;
	auto viSizeStr = first_numberstring(viDump);
	auto viSize = atoi(viSizeStr.data());

	auto cb = ([viSize, callbackPercentage](int32_t progress) {
		callbackPercentage((float)progress / viSize);
	});

	// Download latest client
	URLDownloadToFileA(0, SKYMP2_ZIP_URL, zipPath_, 0, cb);
	return zipPath_;
}

void SaveCommitSha(std::wstring commitSha) {
	wchar_t tempPath[MAX_PATH] = L"";
	GetTempPathW(MAX_PATH, tempPath);
	static auto fileName = toUtf16LE(TEMP_FILE_6);
	swprintf_s(tempPath, L"%s/%s", tempPath, fileName.data());
	std::wofstream(tempPath) << commitSha;
}

std::wstring GetCommitSha() {
	wchar_t tempPath[MAX_PATH] = L"";
	GetTempPathW(MAX_PATH, tempPath);
	static auto fileName = toUtf16LE(TEMP_FILE_6);
	swprintf_s(tempPath, L"%s/%s", tempPath, fileName.data());
	auto f = std::wifstream(tempPath);
	if (f.good() == false) return L"";
	std::wstring result;
	f >> result;
	return result;
}

// https://stackoverflow.com/questions/17933917/get-the-users-desktop-folder-using-windows-api
LPSTR desktop_directory_ansi() {
	static char path[MAX_PATH + 1];
	if (SHGetSpecialFolderPathA(HWND_DESKTOP, path, CSIDL_DESKTOP, FALSE)) return path;
	return (char *)"ERROR";
}

void UnzipSkympZip(const char *zipPath, const char *dest, std::function<void(std::wstring)> outCommitSha) {
	// Unzip client files
	const auto wZipPath = toUtf16LE(zipPath);
	const auto wUnzipPath = toUtf16LE(dest);
	HZIP hz = OpenZip(wZipPath.data(), 0);
	ZIPENTRY ze; GetZipItem(hz, -1, &ze); int numitems = ze.index;
	// -1 gives overall information about the zipfile

	std::basic_string<TCHAR> folderInsideZipName; // "skymp2-client-binaries-master-7ce28fb93261de0c4e5e9a3c2e9588884df7f3d9/"
	for (int zi = 0; zi<numitems; zi++) {
		ZIPENTRY ze; GetZipItem(hz, zi, &ze); // fetch individual details

		if (folderInsideZipName.empty()) {
			folderInsideZipName = ze.name;
			if (outCommitSha) {
				auto commitName = folderInsideZipName.substr(folderInsideZipName.find_last_of(L"-") + 1);
				commitName.pop_back();
				outCommitSha(commitName);
			}
		}
		else {
			TCHAR fullPathBuf[MAX_PATH] = _T("");
			swprintf_s(fullPathBuf, _T("%s/%s"), wUnzipPath.data(), ze.name + 1 + std::basic_string<TCHAR>(ze.name).find('/'));
			UnzipItem(hz, zi, fullPathBuf);         // e.g. the item's name.
		}
	}
	CloseZip(hz);
}

// Throws!
void Compare(const char *commit, bool *outStructureChanged, std::vector<std::string> *outChangedPaths) {
	assert(outStructureChanged);
	assert(outChangedPaths);

	char url[1024];
	sprintf_s(url, SKYMP2_COMPARE_URL_FMT, commit, SKYMP2_MASTER_BRANCH);

	static char tempPath[MAX_PATH] = "";
	if (!tempPath[0]) {
		GetTempPathA(MAX_PATH, tempPath);
		sprintf_s(tempPath, "%s/" TEMP_FILE_5, tempPath);
	}
	URLDownloadToFileA(0, url, tempPath, 0, 0);
	
	std::ifstream t(tempPath);
	const std::string compareDump((std::istreambuf_iterator<char>(t)), 
		std::istreambuf_iterator<char>());
	try {
		auto compare = json::parse(compareDump);
		if (compare.count("diffs") == 0) {
			throw std::logic_error("diffs not found in " + compareDump + "(commit=" + commit + ")");
		}
		auto diffs = compare.at("diffs");
		for (size_t i = 0; i < diffs.size(); ++i) {
			auto &diff = diffs[i];
			const auto old_path = diff.at("old_path").get<std::string>();
			const auto new_path = diff.at("new_path").get<std::string>();
			if (old_path != new_path) {
				*outStructureChanged = true;
				outChangedPaths->clear();
				return;
			}
			outChangedPaths->push_back(new_path);
		}
	}
	catch (json::parse_error &e) {
		throw std::runtime_error(std::string("Lox error in Compare(): ") + e.what());
	}
}

bool IsValidTESVFolder() {
	return std::wifstream(toUtf16LE(g_tesvFolder) + L"\\TESV.exe").good();
}

void PlayUISound(const char *name) {
	char tempPath[MAX_PATH];
	GetTempPathA(MAX_PATH, tempPath);

	char path[MAX_PATH];
	sprintf_s(path, "%s/%s", tempPath, name);
	PlaySoundA(path, 0, SND_FILENAME | SND_ASYNC);
}

// ImGui Helpers
void Font(ImFont *font, std::function<void()> f) {
	ImGui::PushFont(font);
	f();
	ImGui::PopFont();
}

static int CALLBACK BrowseCallbackProc(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{

	if (uMsg == BFFM_INITIALIZED)
	{
		//std::string tmp = (const char *)lpData;
		//std::cout << "path: " << tmp << std::endl;
		//MessageBoxA(hwnd, (char *)lpData, "lol", MB_ICONINFORMATION);
		SendMessage(hwnd, BFFM_SETSELECTION, TRUE, lpData);
	}

	return 0;
}

TCHAR *BrowseFolder(const char *path_param)
{
	static TCHAR path[MAX_PATH];

	BROWSEINFO bi = { 0 };
	bi.lpszTitle = ((L"Укажите путь до корневой папки игры TES V: Skyrim"));
	bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;
	bi.lpfn = BrowseCallbackProc;
	bi.lParam = (LPARAM)path_param;

	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);

	if (pidl != 0)
	{
		//get the name of the folder and put it in path
		SHGetPathFromIDList(pidl, path);

		//free memory used
		IMalloc * imalloc = 0;
		if (SUCCEEDED(SHGetMalloc(&imalloc)))
		{
			imalloc->Free(pidl);
			imalloc->Release();
		}

		return path;
	}


	return nullptr;
}

void windows_system(const std::wstring &cmdS, const wchar_t *) {
	auto cmd = cmdS.data();

	PROCESS_INFORMATION p_info;
	STARTUPINFO s_info;

	memset(&s_info, 0, sizeof(s_info));
	memset(&p_info, 0, sizeof(p_info));
	s_info.cb = sizeof(s_info);

	auto cmdline = _tcsdup((cmd));
	auto programpath = _tcsdup((cmd));

	auto res = (CreateProcess(programpath, cmdline, NULL, NULL, 0, 0, NULL, NULL, &s_info, &p_info));
	Log("windows_system() CreateProcess " + std::to_string(res));
}

#include <ShellAPI.h>

void OnButtonClick(int button) {
	if (button == BUTTON_BACK) {
		PlayUISound("ui_map_rollover.wav");
		if (g_pageIndex > 0) g_pageIndex--;
	}
	else if (button == BUTTON_NEXT) {
		PlayUISound("ui_map_rollover.wav");
		if (g_pageIndex + 1 < (int)std::size(g_pages)) g_pageIndex++;
	}
	else if (button == BUTTON_FINISH) {
		g_pages[g_pageIndex].blocked[BUTTON_FINISH] = true;
		PlayUISound("ui_menu_ok");
		std::thread([] {
			if (g_startAfterInstall && g_pageIndex == kPageId_Progress) {
				auto folder = toUtf16LE(g_tesvFolder);
				// Не смог нормально запустить к сожалению
				windows_system(folder + L"\\TESV.exe", nullptr);
				WriteLaunchMoment(MAGIC_LAUNCH_MOMENT);
				///windows_system(folder + L"\\SkyMP.exe", folder.data());
				///char cmd[256];
				///sprintf_s(cmd, "cd %s && SkyMP.exe", g_tesvFolder);
				///system(cmd);
				///char exec[MAX_PATH];
				///sprintf_s(exec, "%s/SkyMP.lnk", desktop_directory_ansi());
				///WinExec(exec, SW_SHOW);
				///MessageBoxA(0, exec, "asd", MB_ICONERROR);
				///ShellExecute(NULL, L"open", (new std::wstring(toUtf16LE(exec)))->data(),
				///	NULL, NULL, SW_SHOWDEFAULT);
			}
			Sleep(600);
			std::exit(0);
		}).detach();
	}
	else if (button == BUTTON_SELECT_FOLDER) {
		auto result = BrowseFolder("/");
		if (result) {
			auto utf8Res = toUtf8(result);
			strcpy(g_tesvFolder, utf8Res.data());
		}
	}
}

void NiceButton(int button, float visibility, std::function<void()> f) {
	auto &alpha = g_alphaBuffers[button];
	for (auto col : { ImGuiCol_Button, ImGuiCol_ButtonActive, ImGuiCol_ButtonHovered }) {
		ImGui::PushStyleColor(col, { alpha, alpha, alpha, visibility });
	}
	ImGui::PushStyleColor(ImGuiCol_Text, { 1 - alpha, 1 - alpha, 1 - alpha, visibility });
	f();
	ImGui::PopStyleColor(4);

	// Calc effect alpha:
	auto &p = g_pages[g_pageIndex];
	if (ImGui::IsItemHovered() && (button >= (int)std::size(p.blocked) || !p.blocked[button])) {
		alpha += 0.1f;
		if (alpha > 1) alpha = 1.f;
	}
	else {
		alpha -= 0.1f;
		if (alpha < 0) alpha = 0.f;
	}
}

void DrawCrashDetectDummy() {
	static char path[MAX_PATH] = { 0 };
	if (!path[0]) {
		GetTempPathA(MAX_PATH, path);
		sprintf_s(path, "%s/%s", path, SKYRIM_SCREEN_JPG);
	}

	assert(g_pd3dDevice);
	static const auto width = (float)GetSystemMetrics(SM_CXSCREEN);
	static const auto height = (float)GetSystemMetrics(SM_CYSCREEN);
	ImGuiUtils::DrawImage(g_pd3dDevice, path, { 0,0 }, width, height, false, "###scren");
	ImGui::Text("OPA");
	Sleep(100);

	if (GetKeyState(VK_ESCAPE) & 0x8000) {
		if (GetKeyState(VK_ESCAPE) & 0x8000) {
			MessageBeep(MB_ICONERROR);
			return std::exit(0);
		}
	}

	assert(g_hwndCrashdetect);
	BringWindowToTop(g_hwndCrashdetect);
}

void Draw();
int LauncherMain();
void DrawCrashDetectNormal() {
	g_pageIndex = kPageId_CrashDetectNormal;
	Draw();
	static bool g_threadStarted = false;
	if (!g_threadStarted) {
		g_threadStarted = true;
		bool inSkyrimFolder = std::ifstream("TESV.exe").good();
		if (!inSkyrimFolder) {
			MessageBoxW(0, L"TESV.exe not found", L"Error", MB_ICONERROR);
			std::exit(0);
		}
		std::thread([] {
			if (GetKeyState(VK_ESCAPE) & 0x8000) {
				return std::exit(0);
			}
			LauncherMain();
			std::exit(0);
		}).detach();
	}
}

auto DrawCrashDetect = CRASHDETECT_DUMMY ? DrawCrashDetectDummy : DrawCrashDetectNormal;

// ImGui render
void Draw() {
	const int flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize;
	ImGui::Begin("#main", nullptr, flags);
	ImGui::SetWindowSize({ g_width - INSTA_BORDER * 2, g_height - INSTA_BORDER * 2 });
	ImGui::SetWindowPos({ INSTA_BORDER, INSTA_BORDER });

	auto &p = g_pages[g_pageIndex];
	Font(g_font24, [&] {
		auto cap = p.caption;
		ImGui::SetCursorPosX((ImGui::GetWindowWidth() - ImGui::CalcTextSize(cap).x) / 2);
		ImGui::Text(cap);
	});
	ImGui::NewLine();
	Font(p.textHeight24 ? g_font24 : g_font14, [&] {
		ImGui::PushTextWrapPos(500);
		ImGui::Text(p.text);
		ImGui::PopTextWrapPos();
	});
	ImGui::NewLine();

	ImGui::SetWindowFontScale(p.fontScale);

	// Custom elements
	switch (g_pageIndex) {
	case kPageId_CrashDetectNormal:
	{
		ImGui::Spinner("#spin", 20, 2, -1);
		return ImGui::End();
	}
	case kPageId_Hello:
		break;
	case kPageId_Folder:
	{
		ImGui::PushItemWidth(ImGui::GetWindowWidth() - INSTA_BUTTON_CHANGE_WIDTH - INSTA_BORDER - 23);
		ImGui::InputText("###tesv_path", g_tesvFolder, sizeof g_tesvFolder, 0);
		int button = BUTTON_SELECT_FOLDER;
		NiceButton(button, 1.f, [&] {
			ImGui::SameLine(ImGui::GetWindowWidth() - INSTA_BUTTON_CHANGE_WIDTH - INSTA_BORDER);
			bool clicked = ImGui::Button(u8"Изменить", { INSTA_BUTTON_CHANGE_WIDTH, INSTA_BUTTON_CHANGE_HEIGHT });
			if (clicked) OnButtonClick(button);
		});
		bool exist = IsValidTESVFolder();
		p.blocked[BUTTON_NEXT] = !exist;
		ImGui::Text((exist || g_tesvFolder[0] == 0) ? u8"" : u8"Неправильная папка (TESV.exe не найден)");
		break;
	}
	case kPageId_Progress:
	{
		static volatile float g_progress = 0.0f;
		static bool g_threadStarted = false;
		static volatile bool g_threadExit = false;

		enum class Progress : uint8_t {
			Checking = 0,
			Downloading = 1,
			Unpacking = 2,
			Finishing = 3,
			Done = 4
		};
		static volatile auto g_progressTextId = Progress::Checking;

		const char *texts[5] = { u8"Проверка конфигурации", u8"Загрузка архива", u8"Распаковка архива", u8"Завершение установки", u8"Поздравляем с успешной установкой" };
		const auto i = (uint8_t)g_progressTextId;
		if (i < std::size(texts)) ImGui::Text(texts[i]);

		static float g_progressOpa = 1.f;
		static float g_prOpaAdd = 0.1f;
		if (g_progressOpa >= 1) {
			g_prOpaAdd = -0.00625f;
		}
		if (g_progressOpa <= 0.7) {
			g_prOpaAdd = 0.025f;
		}
		g_progressOpa += g_prOpaAdd;

		// Progress bar
		ImGui::PushStyleColor(ImGuiCol_PlotHistogram, { 1,1,1, g_progressOpa });
		ImGui::PushStyleColor(ImGuiCol_FrameBg, { 0.3f, 0.3f, 0.3f, 1.f });
		float progress = g_progress;
		static float g_progressPretty = 0.0f;
		float v = g_progressPretty > 0.90f ? 0.00f : 1.00f;
		ImGui::PushStyleColor(ImGuiCol_Text, { v, v, v, 1.f });
		if (progress > g_progressPretty) g_progressPretty += 0.02f;
		g_progressPretty = min(g_progressPretty, 1.00f);
		if (g_progressTextId != Progress::Done && g_progressPretty >= 1.00f) {
			g_progressPretty = 0.99f;
		}
		ImGui::ProgressBar(g_progressPretty);
		ImGui::PopStyleColor(3);

		// "Start SkyMP after install"
		Font(g_font14, [] {
			ImGui::NewLine();
			ImGui::Checkbox(u8"Запустить SkyMP по завершению установки", &g_startAfterInstall);
			static bool g_was = true;
			if (g_was != g_startAfterInstall) {
				g_was = g_startAfterInstall;
				PlayUISound(g_startAfterInstall ? "ui_select_on" : "ui_select_off");
			}
		});

		if (!g_threadStarted) {
			g_threadStarted = true;
			std::thread([] {
				g_progressTextId = Progress::Downloading;
				Log("DownloadSkympZip started");
				clock_t ms = clock();
				auto zipPath = DownloadSkympZip(nullptr, [](float progress) { g_progress = progress; });
				ms = clock() - ms;
				Log("DownloadSkympZip finished in " + std::to_string(ms) + " ms");
				g_progressTextId = Progress::Unpacking;
				g_progressTextId = Progress::Finishing;
				UnzipSkympZip(zipPath, g_tesvFolder, SaveCommitSha);

				
				char old[MAX_PATH], neww[MAX_PATH];
				sprintf_s(old, "%s/SkyMP.exe.tmp", g_tesvFolder);
				sprintf_s(neww, "%s/SkyMP.exe", g_tesvFolder);
				BOOL resDel = DeleteFileA(neww);
				Log("DeleteFileA('" + (std::string)neww + "') " + std::to_string(resDel));
				int renameRes = rename(old, neww);
				Log("rename(" + (std::string)old + ", " + neww + ") " + std::to_string(renameRes));
				
				g_progressTextId = Progress::Done;
				g_threadExit = true;
			}).detach();
		}
		if (g_threadExit) {
			g_threadExit = false;

			// http://libraryofcprograms.blogspot.com/2014/08/get-desktop-path-dynamically-in-c.html
			TCHAR path[_MAX_PATH] = _T(""); // must be _MAX_PATH in size
			HRESULT hr = SHGetFolderPath(NULL, CSIDL_DESKTOP, 0, NULL, path);

			if (SUCCEEDED(hr)) {
				// Create .lnk file on Desktop
				CreateShortCut csc;
				auto lnkBasePath = toUtf16LE(g_tesvFolder) + L"\\SkyMP.exe";
				csc.CreateLinkFileBase(lnkBasePath.data(),
					path, L"Immersive Skyrim MMO Experience",
					MAKEWORD(0x41, HOTKEYF_ALT + HOTKEYF_CONTROL), _T(CMDLINE_LAUNCHER));
			}
			else 
				MessageBoxA(NULL, "Не удалось создать ярлык. SkyMP.exe помещён в папку с игрой.", "Ошибка", MB_ICONERROR);

			// Allow finish
			p.blocked[BUTTON_FINISH] = false;
		}
	}
	default:
		break;
	}

	const auto n = std::size(p.buttons);
	const auto magic = 40; // Somehow depends on button width
	const auto totalSpace = (ImGui::GetWindowWidth() - n * BUTTON_WIDTH) - magic;
	const float cursorX[3] = { 50, 200, 430 };
	for (size_t i = 0; i != n; ++i) {

		ImGui::SameLine(); 
		ImGui::SetCursorPosY(INSTA_HEIGHT - BUTTON_HEIGHT - SPACE_HEIGHT - INSTA_BORDER - INSTA_BORDER);
		// align left: SetCursorPosX(SPACE_WIDTH + i * (SPACE_WIDTH + BUTTON_WIDTH))
		ImGui::SetCursorPosX(cursorX[i]);

		if (!p.drawButton[i]) continue;
		const float visibility = p.blocked[i] ? 0.7f : 1.f;
		NiceButton(i, visibility, [&] {
			if (ImGui::Button(p.buttons[i], { BUTTON_WIDTH, BUTTON_HEIGHT }) && !p.blocked[i]) {
				OnButtonClick(i);
			}
		});
	}
	
	ImGui::End();
}

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam))
		return true;

	switch (msg)
	{
	case WM_SIZE:
		if (g_pd3dDevice != NULL && wParam != SIZE_MINIMIZED)
		{
			ImGui_ImplDX9_InvalidateDeviceObjects();
			g_d3dpp.BackBufferWidth = LOWORD(lParam);
			g_d3dpp.BackBufferHeight = HIWORD(lParam);
			HRESULT hr = g_pd3dDevice->Reset(&g_d3dpp);
			if (hr == D3DERR_INVALIDCALL)
				IM_ASSERT(0);
			ImGui_ImplDX9_CreateDeviceObjects();
		}
		return 0;
	case WM_SYSCOMMAND:
		if ((wParam & 0xfff0) == SC_KEYMENU) // Disable ALT application menu
			return 0;
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

void StyleColorsSkyMP(ImGuiStyle* dst = 0)
{
	ImGuiStyle* style = dst ? dst : &ImGui::GetStyle();
	ImVec4* colors = style->Colors;

	colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
	colors[ImGuiCol_TextDisabled] = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
	colors[ImGuiCol_WindowBg] = ImVec4(0.07f, 0.07f, 0.07f, 1);
	colors[ImGuiCol_ChildBg] = ImVec4(1.00f, 1.00f, 1.00f, 0.00f);
	colors[ImGuiCol_PopupBg] = ImVec4(0.08f, 0.08f, 0.08f, 0.94f);
	colors[ImGuiCol_Border] = ImVec4(0.43f, 0.43f, 0.50f, 0.00f);
	colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
	colors[ImGuiCol_FrameBg] = ImVec4(0.16f, 0.29f, 0.48f, 0.54f);
	colors[ImGuiCol_FrameBgHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);
	colors[ImGuiCol_FrameBgActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
	colors[ImGuiCol_TitleBg] = ImVec4(0.04f, 0.04f, 0.04f, 1.00f);
	colors[ImGuiCol_TitleBgActive] = ImVec4(0.16f, 0.29f, 0.48f, 1.00f);
	colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 0.51f);
	colors[ImGuiCol_MenuBarBg] = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
	colors[ImGuiCol_ScrollbarBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.53f);
	colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.31f, 0.31f, 0.31f, 1.00f);
	colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.41f, 0.41f, 0.41f, 1.00f);
	colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
	colors[ImGuiCol_CheckMark] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_SliderGrab] = ImVec4(0.24f, 0.52f, 0.88f, 1.00f);
	colors[ImGuiCol_SliderGrabActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_Button] = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);
	colors[ImGuiCol_ButtonHovered] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_ButtonActive] = ImVec4(0.06f, 0.53f, 0.98f, 1.00f);
	colors[ImGuiCol_Header] = ImVec4(0.26f, 0.59f, 0.98f, 0.31f);
	colors[ImGuiCol_HeaderHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.80f);
	colors[ImGuiCol_HeaderActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_Separator] = colors[ImGuiCol_Border];
	colors[ImGuiCol_SeparatorHovered] = ImVec4(0.10f, 0.40f, 0.75f, 0.78f);
	colors[ImGuiCol_SeparatorActive] = ImVec4(0.10f, 0.40f, 0.75f, 1.00f);
	colors[ImGuiCol_ResizeGrip] = ImVec4(0.26f, 0.59f, 0.98f, 0.25f);
	colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
	colors[ImGuiCol_ResizeGripActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.95f);
	colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
	colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
	colors[ImGuiCol_PlotHistogram] = ImVec4(1, 1, 1, 1.00f); // Progress finish
	colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
	colors[ImGuiCol_TextSelectedBg] = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
	colors[ImGuiCol_DragDropTarget] = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
	colors[ImGuiCol_NavHighlight] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
	colors[ImGuiCol_NavWindowingDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.20f);
	colors[ImGuiCol_ModalWindowDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);
}

typedef void(*VoidFn)();
void StyleVarsSkymp(VoidFn f) {
	ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0);
	f();
	ImGui::PopStyleVar();
}

void ExtractSounds() {
	char tempPath[MAX_PATH];
	GetTempPathA(MAX_PATH, tempPath);

	char fontPath[MAX_PATH];
	sprintf_s(fontPath, "%s/" TEMP_FILE_4, tempPath);

	FILE *write_ptr;
	write_ptr = fopen(fontPath, "wb");  // w for write, b for binary
	fwrite(sounds_zip, std::size(sounds_zip), 1, write_ptr);
	fclose(write_ptr);

	auto wFontPath = toUtf16LE(fontPath);
	auto wUnzipPath = toUtf16LE(tempPath);
	HZIP hz = OpenZip(wFontPath.data(), 0);
	ZIPENTRY ze; GetZipItem(hz, -1, &ze); int numitems = ze.index;
	// -1 gives overall information about the zipfile

	for (int zi = 0; zi<numitems; zi++) {
		ZIPENTRY ze; GetZipItem(hz, zi, &ze); // fetch individual details
		TCHAR fullPathBuf[MAX_PATH] = _T("");
		swprintf_s(fullPathBuf, _T("%s/%s"), wUnzipPath.data(), ze.name);
		UnzipItem(hz, zi, fullPathBuf);         // e.g. the item's name.
	}
	CloseZip(hz);
}

__forceinline void Debug(const char *msg) {
	//MessageBoxA(0, msg, "Debug", MB_ICONINFORMATION);
}

#include <tlhelp32.h>

DWORD GetProcessIdByName(std::vector<const wchar_t *> names) {
	const DWORD myPid = GetCurrentProcessId();
	DWORD pid = 0;
	// Create toolhelp snapshot.
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 process;
	ZeroMemory(&process, sizeof(process));
	process.dwSize = sizeof(process);
	// Walkthrough all processes.
	if (Process32First(snapshot, &process)) {
		do {
			// Compare process.szExeFile based on format of name, i.e., trim file path
			// trim .exe if necessary, etc.
			if (process.th32ProcessID != myPid) {
				for (auto name : names) {
					if (!wcscmp(process.szExeFile, name)) {
						pid = process.th32ProcessID;
					}
				}
			}
		} while (Process32Next(snapshot, &process));
	}
	CloseHandle(snapshot);
	return pid;
}

int LauncherMain() {
	auto appdata = getenv("APPDATA");
	if (appdata && appdata[0]) {
		std::string appdataLocal = appdata;
		appdataLocal = { appdataLocal.begin(), appdataLocal.end() - strlen("/Roaming")};
		for (auto fileName : { "plugins.txt"/*, "DLCList.txt"*/ }) {
			auto p = appdataLocal + (std::string)"/Local/Skyrim/" + fileName;

			std::ofstream f(p);
			f << "Dragonborn.esm\nDawnguard.esm\nHearthFires.esm\nSkyMP.esp\nSkyUI.esp" << std::endl;
		}
	}
	else {
		Log("Bad APPDATA env");
		MessageBoxA(0, "Не удалось автоматически подключить SkyMP.esp", "Warning", MB_ICONWARNING);
	}

	g_tesvFolder[0] = 0;
	if (std::ifstream("SkyMP.exe.inf").good()) {
		BOOL res = DeleteFileA("SkyMP.exe.inf");
		Log("DeleteFileA('SkyMP.exe.inf') " + std::to_string(res));
	}
	else {
		static const std::vector<const wchar_t *> v = { L"SkyMP.exe", L"TESV.exe", L"skse_loader.exe", L"skymp2-installer.exe" };
		auto pid = GetProcessIdByName(v);
		Log("pid = " + std::to_string(pid));
		for (auto exeName : v) {
			auto _pid = GetProcessIdByName({ exeName });
			Log(toUtf8(exeName) + " pid = " + std::to_string(_pid));
		}
		if (pid) {
			Log("pid != 0, exiting");
			return MessageBeep(MB_ICONERROR);
		}
	}

	auto currentCommit = toUtf8(GetCommitSha());

	bool structChanged = false;
	std::vector<std::string> paths;
	Compare(currentCommit.data(), &structChanged, &paths);

	std::string s;
	for (auto &p : paths) s += p + ", ";
	
	s = "{" + s + "}";
	
	s += "\nstructChanged=" + std::to_string(structChanged);
	s += "\ncurrentCommit.empty=" + std::to_string(currentCommit.empty());
	Log(s.data());

	bool installFull = currentCommit.empty() || structChanged;
	if (installFull) {
		auto zipPath = DownloadSkympZip("", [](float) {});
		UnzipSkympZip(zipPath, ".", SaveCommitSha);
		Log("Installing finished");
	}
	else {
		for (auto &path : paths) {
			char url[512], fullPath[MAX_PATH];
			GetCurrentDirectoryA(MAX_PATH, fullPath);
			sprintf_s(fullPath, "%s/%s", fullPath, path.data());
			sprintf_s(url, "%s/%s", SKYMP2_RAW_URL, path.data());
			URLDownloadToFileA(0, url, fullPath, 0, 0);

			char downloadInfo[256];
			sprintf_s(downloadInfo, "URL: %s\nPath: %s", url, fullPath);
			Log(downloadInfo);
		}
		char msg[128];
		sprintf_s(msg, "Updated %d files", (int)paths.size());
		Log(msg);
		if (paths.size()) {
			char ukusilPath[MAX_PATH];
			GetCurrentDirectoryA(MAX_PATH, ukusilPath);
			sprintf_s(ukusilPath, "%s/%s", ukusilPath, "ukusil.tmp");

			int res = 0;
			std::ifstream("check_res.tmp") >> res;
			if (std::ifstream("check_res.tmp").good() == false || !res) {
				std::ofstream(ukusilPath) << "MP";
			}
		}
	}

	Log("Branches info - Downloading");
	// Download branches info
	json branches;
	{
		char tempPath[MAX_PATH];
		GetTempPathA(MAX_PATH, tempPath);
		sprintf_s(tempPath, "%s/%s", tempPath, TEMP_FILE_7);
		URLDownloadToFileA(0, SKYMP2_BRANCHES_URL, tempPath, 0, 0);
		std::ifstream t(tempPath);
		std::string branchesDump((std::istreambuf_iterator<char>(t)),
			std::istreambuf_iterator<char>());
		try {
			branches = json::parse(branchesDump);
		}
		catch (json::parse_error &e) {
			throw std::runtime_error(std::string("Lox error in LauncherMain(): ") + e.what());
		}
	}
	Log("Branches info - Done");
	///Log(branches.dump(2));
	for (size_t i = 0; i < branches.size(); ++i) {
		auto &branch = branches[i];
		auto name = branch.at("name").get<std::string>();
		auto commitId = branch.at("commit").at("id").get<std::string>();
		if (name == SKYMP2_MASTER_BRANCH) {
			SaveCommitSha(toUtf16LE(commitId));
			commitId = "Saved commit sha: " + commitId;
			Log(commitId.data());
			break;
		}
	}


	// Show launcher is here for SKYMP2.asi
	{
		WriteLaunchMoment(time(0));
	}

	if (std::ifstream("skse_loader.exe").good() == false) {
		MessageBoxA(0, "Отсутствует файл skse_loader.exe\nВозможно, SKSE установлен неправильно или отсутствует. Вы будете переведены на страницу SKSE в Steam.", "Ошибка", MB_ICONWARNING);
		ShellExecuteW(0, 0, L"https://store.steampowered.com/app/365720/", 0, 0, SW_SHOW);
		return -1;
	}

	STARTUPINFOA cif;
	PROCESS_INFORMATION pi;
	ZeroMemory(&cif, sizeof(decltype(cif)));
	auto res = CreateProcessA("skse_loader.exe", NULL,
		NULL, NULL, FALSE, DETACHED_PROCESS | NORMAL_PRIORITY_CLASS, NULL, NULL, &cif, &pi) == TRUE;
	Log("LauncherMain() CreateProcess DETACHED_PROCESS | NORMAL_PRIORITY_CLASS" + std::to_string(res));
	if (res == 0) {
		Log("GetLastError = " + std::to_string(GetLastError()));

		res = CreateProcessA("skse_loader.exe", NULL,
			NULL, NULL, FALSE, DETACHED_PROCESS, NULL, NULL, &cif, &pi) == TRUE;
		Log("LauncherMain() CreateProcess DETACHED_PROCESS" + std::to_string(res));
		if (res == 0) {
			Log("GetLastError = " + std::to_string(GetLastError()));

			res = CreateProcessA("skse_loader.exe", NULL,
				NULL, NULL, FALSE, NULL, NULL, NULL, &cif, &pi) == TRUE;
			Log("LauncherMain() CreateProcess NULL" + std::to_string(res));
			if (res == 0) {
				Log("GetLastError = " + std::to_string(GetLastError()));

				// Skyrim is 72850
				Log("LauncherMain() ShellExecuteW" + std::to_string(res));
				if (ERROR_SUCCESS != ShellExecuteW(0, 0, L"steam://rungameid/365720", 0, 0, SW_SHOW)) {
					Log("Unable to start the game!");
					WriteLaunchMoment(MAGIC_LAUNCH_MOMENT);
					MessageBoxA(0, "Не удалось запустить SkyMP автоматически. Пожалуйста, запуститe TES V: Skyrim через Steam (ещё раз), чтобы поиграть в SkyMP.", "Ошибка", MB_ICONERROR);
				}
			}
		}
	}
	return 0;
}

/*********************************************************************
* Function......: ResolveShortcut
* Parameters....: lpszShortcutPath - string that specifies a path
and file name of a shortcut
*          lpszFilePath - string that will contain a file name
* Returns.......: S_OK on success, error code on failure
* Description...: Resolves a Shell link object (shortcut)
*********************************************************************/
HRESULT ResolveShortcut(/*in*/ LPCTSTR lpszShortcutPath,
	/*out*/ LPTSTR lpszFilePath)
{
	HRESULT hRes = E_FAIL;
	IShellLink*    psl = NULL;

	// buffer that receives the null-terminated string
	// for the drive and path
	TCHAR szPath[MAX_PATH];
	// buffer that receives the null-terminated
	// string for the description
	TCHAR szDesc[MAX_PATH];
	// structure that receives the information about the shortcut
	WIN32_FIND_DATA wfd;
	WCHAR wszTemp[MAX_PATH];

	lpszFilePath[0] = '\0';

	// Get a pointer to the IShellLink interface
	hRes = CoCreateInstance(CLSID_ShellLink,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_IShellLink,
		(void**)&psl);

	if (SUCCEEDED(hRes))
	{
		// Get a pointer to the IPersistFile interface
		IPersistFile*  ppf = NULL;
		psl->QueryInterface(IID_IPersistFile, (void **)&ppf);


		// IPersistFile is using LPCOLESTR,
		// so make sure that the string is Unicode
#if !defined _UNICODE
		MultiByteToWideChar(CP_ACP, 0, lpszShortcutPath,
			-1, wszTemp, MAX_PATH);
#else
		wcsncpy(wszTemp, lpszShortcutPath, MAX_PATH);
#endif

		// Open the shortcut file and initialize it from its contents
		hRes = ppf->Load(wszTemp, STGM_READ);
		if (SUCCEEDED(hRes))
		{
			// Try to find the target of a shortcut,
			// even if it has been moved or renamed
			hRes = psl->Resolve(NULL, SLR_UPDATE);
			if (SUCCEEDED(hRes))
			{
				// Get the path to the shortcut target
				hRes = psl->GetPath(szPath,
					MAX_PATH, &wfd, SLGP_RAWPATH);
				if (FAILED(hRes))
					return hRes;

				// Get the description of the target
				hRes = psl->GetDescription(szDesc,
					MAX_PATH);
				if (FAILED(hRes))
					return hRes;

				lstrcpyn(lpszFilePath, szPath, MAX_PATH);

			}
		}
	}

	return hRes;
}

#include <cstdio>
#include <windows.h>
#include <tlhelp32.h>

// https://stackoverflow.com/questions/865152/how-can-i-get-a-process-handle-by-its-name-in-c
bool ForTESV(std::function<void(HANDLE)> f) {
	bool tesvFound = false;
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	if (Process32First(snapshot, &entry) == TRUE) {
		while (Process32Next(snapshot, &entry) == TRUE) {
			if (std::wstring(entry.szExeFile) == L"TESV.exe") {
				HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);
				f(hProcess);
				tesvFound = true;
				CloseHandle(hProcess);
			}
		}
	}
	CloseHandle(snapshot);
	return tesvFound;
}

int WINAPI WinMain
(

	HINSTANCE hInstance,		// дескриптор текущего экземпляра окна
	HINSTANCE hPrevInstance,		// дескриптор предыдущего экземпляра окна 
	LPSTR lpCmdLine,			// указатель на командную строку
	int nCmdShow 			// показывает состояние окна 
)
{
	if (!strcmp(lpCmdLine, CMDLINE_LAUNCHER)) {
		TCHAR fileName[MAX_PATH];
		DWORD size = GetModuleFileName(GetModuleHandle(NULL), fileName, MAX_PATH);
		for (int i = size - 1; i >= 0; --i) {
			auto ch = fileName[i];
			fileName[i] = 0;
			if (ch == _T('/') || ch == _T('\\')) {
				break;
			}
		}
		Log("SetCurrentDirectory " + toUtf8(fileName));
		SetCurrentDirectory(fileName);
	}

	// Crash Detector
	g_isCrashdetect = !strcmp(lpCmdLine, CMDLINE_CRASHDETECT);
	if (g_isCrashdetect) {
		Log("g_isCrashdetect == true");
		g_width = CRASHDETECT_WIDTH;
		g_height = CRASHDETECT_HEIGHT;
		bool skyrimSeen = false;
		while (1) {
			bool tesvFound = ForTESV([](HANDLE hProcess) {
				WaitForSingleObject(hProcess, INFINITE);
				enum {
					INVALID = 228,
					UPDATED = 108
				};
				DWORD exitCode = INVALID;
				BOOL suc = GetExitCodeProcess(hProcess, &exitCode);
				Log("GetExitCodeProcess " + std::to_string(suc));
				Log("exited with " + std::to_string(exitCode));
				if (exitCode == 0 || exitCode == 1 || exitCode == -1) return std::exit(0); // No restart on normal exit
				if (exitCode == UPDATED) g_pages[kPageId_CrashDetectNormal].caption = u8"Завершение установки обновлений";

				char path[MAX_PATH] = { 0 };
				if (path[0] == 0) {
					GetTempPathA(MAX_PATH, path);
					sprintf_s(path, "%s/%s", path, "SkympTick.tmp");
				}
				std::ifstream f(path);
				bool good = f.good();
				time_t t = 0;
				f >> t;
				int secondsPassed = time(0) - t;
				auto s = std::to_string(secondsPassed);
				if (!good || secondsPassed > 2) return std::exit(0); // No restart if main thread was not responding
				std::ofstream("_crashed.tmp") << 1; // Notify game we need to skip auth
			});
			if (tesvFound) break;
		}
	}

	// Launcher
	bool inSkyrimFolder = std::ifstream("TESV.exe").good();
	if (!g_isCrashdetect && inSkyrimFolder) {
		try {
			Log("LauncherMain start");
			return LauncherMain();
		}
		catch (std::exception &e) {
			Log("Fatal Error: " + std::string(e.what()));
			MessageBoxA(0, e.what(), "SkyMP.exe Fatal Error", MB_ICONERROR);
			return 0;
		}
	}

	ExtractSounds();
	// Create application window
	WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, WndProc, 0L, 0L, GetModuleHandle(NULL), NULL, NULL, NULL, NULL, _T("ImGui Example"), NULL };
	RegisterClassEx(&wc);
	HWND hwnd = CreateWindowEx(WS_EX_APPWINDOW | (g_isCrashdetect ? WS_EX_PALETTEWINDOW : 0),
			_T("ImGui Example"),
			_T(INSTA_CAPTION),
			WS_POPUP,
			INSTA_X, INSTA_Y,
			g_width, g_height,
			NULL,
			NULL,
			hInstance,
			NULL
		);
	if (g_isCrashdetect && CRASHDETECT_DUMMY) {
		// Go fullscreen like Chromium
		// https://stackoverflow.com/questions/2382464/win32-full-screen-and-hiding-taskbar
		HMONITOR hmon = MonitorFromWindow(hwnd,
			MONITOR_DEFAULTTONEAREST);
		MONITORINFO mi = { sizeof(mi) };
		if (!GetMonitorInfo(hmon, &mi)) return NULL;
		hwnd = CreateWindow(TEXT("static"),
			TEXT("something interesting might go here"),
			WS_POPUP | WS_VISIBLE,
			mi.rcMonitor.left,
			mi.rcMonitor.top,
			mi.rcMonitor.right - mi.rcMonitor.left,
			mi.rcMonitor.bottom - mi.rcMonitor.top,
			hwnd, NULL, GetModuleHandle(NULL), 0);
		g_hwndCrashdetect = hwnd;
		Log("Created fullscreen window");
	}

	SetWindowLong(hwnd, GWL_EXSTYLE,
		GetWindowLong(hwnd, GWL_EXSTYLE) | WS_EX_LAYERED);
	SetLayeredWindowAttributes(hwnd, 0, (255 * INSTA_ALPHA) / 100, LWA_ALPHA);

	// https://stackoverflow.com/questions/2398746/removing-window-border
	SetWindowLong(hwnd /*The handle of the window to remove its borders*/, GWL_STYLE, WS_POPUP);
	SetWindowPos(hwnd, NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);

	// Initialize Direct3D
	LPDIRECT3D9 pD3D;
	if ((pD3D = Direct3DCreate9(D3D_SDK_VERSION)) == NULL)
	{
		UnregisterClass(_T("ImGui Example"), wc.hInstance);
		return 0;
	}
	ZeroMemory(&g_d3dpp, sizeof(g_d3dpp));
	g_d3dpp.Windowed = TRUE;
	g_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	g_d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	g_d3dpp.EnableAutoDepthStencil = TRUE;
	g_d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	g_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE; // Present with vsync
															//g_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE; // Present without vsync, maximum unthrottled framerate

															// Create the D3DDevice
	if (pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hwnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &g_d3dpp, &g_pd3dDevice) < 0)
	{
		pD3D->Release();
		UnregisterClass(_T("ImGui Example"), wc.hInstance);
		return 0;
	}

	// Setup Dear ImGui binding
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
	ImGui_ImplWin32_Init(hwnd);
	ImGui_ImplDX9_Init(g_pd3dDevice);

	// Setup style
	StyleColorsSkyMP();

	//ImGui::StyleColorsClassic();

	// Load Fonts
	// - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them. 
	// - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple. 
	// - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
	// - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
	// - Read 'misc/fonts/README.txt' for more instructions and details.
	// - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
	//io.Fonts->AddFontDefault();
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
	//ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
	//IM_ASSERT(font != NULL);

	char tempPath[MAX_PATH];
	GetTempPathA(MAX_PATH, tempPath);

	char fontPath[MAX_PATH];
	sprintf_s(fontPath, "%s/" TEMP_FILE_1, tempPath);

	FILE *write_ptr;
	write_ptr = fopen(fontPath, "wb");  // w for write, b for binary
	fwrite(futuralightc_otf, std::size(futuralightc_otf), 1, write_ptr); // write 10 bytes from our buffer
	fclose(write_ptr);

	g_font24 = io.Fonts->AddFontFromFileTTF(fontPath, 24, NULL, io.Fonts->GetGlyphRangesCyrillic());
	g_font14 = io.Fonts->AddFontFromFileTTF(fontPath, 16, NULL, io.Fonts->GetGlyphRangesCyrillic());

	// Main loop
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	ShowWindow(hwnd, SW_SHOWDEFAULT);
	UpdateWindow(hwnd);
	while (msg.message != WM_QUIT)
	{
		// Poll and handle messages (inputs, window resize, etc.)
		// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
		// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
		// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
		// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			continue;
		}

		// Start the Dear ImGui frame
		ImGui_ImplDX9_NewFrame();
		ImGui_ImplWin32_NewFrame();
		ImGui::NewFrame();

		StyleVarsSkymp(g_isCrashdetect ? DrawCrashDetect : Draw);

		// Rendering
		ImGui::EndFrame();
		g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, false);
		g_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
		g_pd3dDevice->SetRenderState(D3DRS_SCISSORTESTENABLE, false);
		D3DCOLOR clear_col_dx = D3DCOLOR_RGBA((int)(clear_color.x*255.0f), (int)(clear_color.y*255.0f), (int)(clear_color.z*255.0f), (int)(clear_color.w*255.0f));
		g_pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, clear_col_dx, 1.0f, 0);
		if (g_pd3dDevice->BeginScene() >= 0)
		{
			ImGui::Render();
			ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
			g_pd3dDevice->EndScene();
		}
		HRESULT result = g_pd3dDevice->Present(NULL, NULL, NULL, NULL);

		// Handle loss of D3D9 device
		if (result == D3DERR_DEVICELOST && g_pd3dDevice->TestCooperativeLevel() == D3DERR_DEVICENOTRESET)
		{
			ImGui_ImplDX9_InvalidateDeviceObjects();
			g_pd3dDevice->Reset(&g_d3dpp);
			ImGui_ImplDX9_CreateDeviceObjects();
		}
	}

	ImGui_ImplDX9_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();

	if (g_pd3dDevice) g_pd3dDevice->Release();
	if (pD3D) pD3D->Release();
	DestroyWindow(hwnd);
	UnregisterClass(_T("ImGui Example"), wc.hInstance);

	return 0;
}
