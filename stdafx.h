// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <shlobj_core.h> // SHBrowseForFolder
#include <Mmsystem.h> // PlaySound

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <ctime>

// C++ standard headers
#include <map>
#include <functional>
#include <fstream>
#include <thread>
#include <string>
#include <mutex>

// Zip Utils
#include "zip_utils/zip.h"
#include "zip_utils/unzip.h"

// Shorcut (.lnk) Utils
#include "shorcut_utils/CreateShortCut.h"

// nlohmann::json
#include "nlohmann/json.h"
using json = nlohmann::json;

// ImGui
#include "imgui/imgui.h"
#include "imgui/imgui_impl_dx9.h"
#include "imgui/imgui_impl_win32.h"
#include "imgui/imgui_internal.h"
#include <d3d9.h>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include <tchar.h>

// .lib
#pragma comment( lib, "D3dx9.lib" )
#pragma comment( lib, "Winmm.lib" )
#pragma comment( lib, "urlmon.lib" )

// D3DXCreateTextureFromFileA
// https://docs.microsoft.com/en-us/windows/desktop/direct3d9/d3dxcreatetexturefromfile
// https://stackoverflow.com/questions/17015088/missing-files-directx-sdk-d3dx9-lib-d3dx9-h
#include <D3dx9tex.h>

// TODO: reference additional headers your program requires here

// https://stackoverflow.com/questions/4631292/how-detect-current-screen-resolution

#define INSTA_WIDTH int(1024 / 1.5)
#define INSTA_HEIGHT int(768 / 1.5)
#define INSTA_X (GetSystemMetrics(SM_CXSCREEN) - INSTA_WIDTH) / 2
#define INSTA_Y (GetSystemMetrics(SM_CYSCREEN) - INSTA_HEIGHT) / 2
#define INSTA_CAPTION "SkyMP"
#define INSTA_ALPHA 100 // 0..100
#define BUTTON_WIDTH 133
#define BUTTON_HEIGHT 42
#define SPACE_WIDTH 8
#define SPACE_HEIGHT 11
#define CRASHDETECT_WIDTH int(1024 / 1.5)
#define CRASHDETECT_HEIGHT 200

// Note: All URLs MUST be without '/' character at the end

#define _URL "gitlab.com"//185.211.246.231:10080
#define PROJ_ID "38394003"//2
#define OWNER "pospelov"//root

//#define _URL "185.197.75.79:10080"//185.211.246.231:10080
//#define PROJ_ID "1"//2
//#define OWNER "root"//root


#define SKYMP2_ZIP_URL "http://"_URL"/api/v4/projects/"PROJ_ID"/repository/archive.zip"
#define SKYMP2_COMPARE_URL_FMT "http://"_URL"/api/v4/projects/"PROJ_ID"/repository/compare?from=%s&to=%s"
#define SKYMP2_BRANCHES_URL "http://"_URL"/api/v4/projects/"PROJ_ID"/repository/branches"
#define SKYMP2_VERINFO_URL "http://"_URL"/"OWNER"/skymp2-binaries/raw/master/version-info.json"
#define SKYMP2_RAW_URL "http://"_URL"/"OWNER"/skymp2-binaries/raw/master"
#define SKYMP2_MASTER_BRANCH "master"

#define INSTA_BORDER 12
#define  INSTA_BUTTON_CHANGE_WIDTH 100
#define  INSTA_BUTTON_CHANGE_HEIGHT 30

// Utils:

namespace ImGuiUtils
{
	inline void BeginInvisible(const char *id, ImVec2 pos, bool focus, bool superSize)
	{
		// Invisible window  
		// https://github.com/ocornut/imgui/issues/530
		ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_WindowBg, ImVec4(0, 0, 0, 0));
		ImGuiWindowFlags flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing;
		//if (focus) flags |= ImGuiWindowFlags_Tooltip | ImGuiWindowFlags_AlwaysUseWindowPadding;

		if (focus) {
			ImGui::SetNextWindowFocus();
		}

		ImGui::Begin(id, nullptr, flags);
		ImGui::SetWindowPos(pos);
		if (superSize) ImGui::SetWindowSize(ImVec2(1000, 1000));
	}

	inline void EndInvisible()
	{
		ImGui::End();
		ImGui::PopStyleColor();
	}

	inline void DrawImage(LPDIRECT3DDEVICE9 device, const char *path, ImVec2 pos, float width, float height, bool focus, const char *id) {
		// Load a texture
		static std::map<std::string, LPDIRECT3DTEXTURE9> my_texture;
		if (!my_texture[path]) {
			while (1) {
				HRESULT res = D3DXCreateTextureFromFileA(device, path, &my_texture[path]);
				//IM_ASSERT(res == 0);
				if (!res) break; // 0 means no errors
				MessageBeep(MB_ICONERROR);
			}
			IM_ASSERT(my_texture[path] != NULL);
		}
		// Retrieve texture info
		D3DSURFACE_DESC my_texture_desc;
		HRESULT res = my_texture[path]->GetLevelDesc(0, &my_texture_desc);
		IM_ASSERT(res == 0);

		ImVec2 uv_min(0.0f, 0.0f);
		ImVec2 uv_max(1.0f, 1.0f);

		BeginInvisible(id ? id : path, pos, focus, false);
		ImGui::Image(my_texture[path], { width, height }, uv_min, uv_max);
		EndInvisible();
	}
}

// https://stackoverflow.com/questions/215963/how-do-you-properly-use-widechartomultibyte
// Convert a wide Unicode string to an UTF8 string
inline std::string toUtf8(const std::wstring &wstr)
{
	if (wstr.empty()) return std::string();
	int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
	std::string strTo(size_needed, 0);
	WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
	return strTo;
}

// Convert an UTF8 string to a wide Unicode String
inline std::wstring toUtf16LE(const std::string &str)
{
	if (str.empty()) return std::wstring();
	int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
	std::wstring wstrTo(size_needed, 0);
	MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
	return wstrTo;
}

class BindStatusCallback : public IBindStatusCallback
{
public:
	using Fn = std::function<void(uint32_t)>;

	Fn fn;

	BindStatusCallback(Fn onProgress) {
		fn = onProgress;
		assert(onProgress);
	}

	virtual HRESULT STDMETHODCALLTYPE QueryInterface(
		/* [in] */ REFIID riid,
		/* [iid_is][out] */ _COM_Outptr_ void __RPC_FAR *__RPC_FAR *ppvObject) {
		return 0;
	};

	virtual ULONG STDMETHODCALLTYPE AddRef(void) { return 0; };

	virtual ULONG STDMETHODCALLTYPE Release(void) { return 0; };

	virtual HRESULT STDMETHODCALLTYPE OnStartBinding(
		/* [in] */ DWORD dwReserved,
		/* [in] */ __RPC__in_opt IBinding *pib) {
		return 0;
	}

	virtual HRESULT STDMETHODCALLTYPE GetPriority(
		/* [out] */ __RPC__out LONG *pnPriority) {
		return 0;
	}

	virtual HRESULT STDMETHODCALLTYPE OnLowResource(
		/* [in] */ DWORD reserved) {
		return 0;
	}

	virtual HRESULT STDMETHODCALLTYPE OnProgress(
		/* [in] */ ULONG ulProgress,
		/* [in] */ ULONG ulProgressMax,
		/* [in] */ ULONG ulStatusCode,
		/* [unique][in] */ __RPC__in_opt LPCWSTR szStatusText) {
		this->fn(ulProgressMax);
		return 0;
	}

	virtual HRESULT STDMETHODCALLTYPE OnStopBinding(
		/* [in] */ HRESULT hresult,
		/* [unique][in] */ __RPC__in_opt LPCWSTR szError) {
		return 0;
	}

	virtual /* [local] */ HRESULT STDMETHODCALLTYPE GetBindInfo(
		/* [out] */ DWORD *grfBINDF,
		/* [unique][out][in] */ BINDINFO *pbindinfo) {
		return 0;
	}

	virtual /* [local] */ HRESULT STDMETHODCALLTYPE OnDataAvailable(
		/* [in] */ DWORD grfBSCF,
		/* [in] */ DWORD dwSize,
		/* [in] */ FORMATETC *pformatetc,
		/* [in] */ STGMEDIUM *pstgmed) {
		return 0;
	}

	virtual HRESULT STDMETHODCALLTYPE OnObjectAvailable(
		/* [in] */ __RPC__in REFIID riid,
		/* [iid_is][in] */ __RPC__in_opt IUnknown *punk) {
		return 0;
	}
};

// https://stackoverflow.com/questions/30073839/c-extract-number-from-the-middle-of-a-string
inline std::string first_numberstring(std::string const & str)
{
	std::size_t const n = str.find_first_of("0123456789");
	if (n != std::string::npos)
	{
		std::size_t const m = str.find_first_not_of("0123456789", n);
		return str.substr(n, m != std::string::npos ? m - n : m);
	}
	return std::string();
}

// https://github.com/ocornut/imgui/issues/1901
namespace ImGui {
	using string = std::string;
	inline void Spinner(const char* label, float radius, int thickness, const ImU32& color) {
		ImGuiWindow* window = GetCurrentWindow();
		if (window->SkipItems)
			return;

		ImGuiContext& g = *GImGui;
		const ImGuiStyle& style = g.Style;
		const ImGuiID id = window->GetID(label);

		ImVec2 pos = window->DC.CursorPos;
		ImVec2 size((radius) * 2, (radius + style.FramePadding.y) * 2);

		const ImRect bb(pos, ImVec2(pos.x + size.x, pos.y + size.y));
		ItemSize(bb, style.FramePadding.y);
		if (!ItemAdd(bb, id))
			return;

		// Render
		window->DrawList->PathClear();

		int num_segments = 30;
		int start = (int)abs(ImSin((float)g.Time*1.8f)*(num_segments - 5));

		float a_minRaw = IM_PI * 2.0f * ((float)start) / (float)num_segments;
		float a_max = IM_PI * 2.0f * ((float)num_segments - 3) / (float)num_segments;

		static std::map<string, float> aminTrans;

		aminTrans[label] += 0.2f * (a_minRaw - aminTrans[label]);

		auto a_min = aminTrans[label];

		const ImVec2 centre = ImVec2(pos.x + radius, pos.y + radius + style.FramePadding.y);

		for (int i = 0; i < num_segments; i++) {
			const float a = a_min + ((float)i / (float)num_segments) * (a_max - a_min);
			window->DrawList->PathLineTo(ImVec2(centre.x + ImCos(a + (float)g.Time * 8) * radius,
				centre.y + ImSin(a + (float)g.Time * 8) * radius));
		}

		window->DrawList->PathStroke(color, false, (float)thickness);
	}
}